################################################################################
#
# dpkg
#
################################################################################

DPKG_VERSION = 1.16.1.2ubuntu7.5
DPKG_SOURCE = dpkg_$(DPKG_VERSION).tar.gz
DPKG_SITE = http://192.168.60.40/dl
DPKG_INSTALL_STAGING=YES
DPKG_INSTALL_TARGET=NO

$(eval $(autotools-package))
