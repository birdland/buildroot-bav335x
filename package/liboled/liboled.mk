##########################################################
##  dashbaord     Birdland Audio dashboard main app 
##########################################################

#BADESK_VERSION = 82ff2efdb9787737b9f21b6f4759f077c827b238
#BADESK_SOURCE = git://gigahub/ba-desk.git
LIBOLED_VERSION = 0.10
LIBOLED_SOURCE = liboled-0.10.tar.gz
LIBOLED_INSTALL_STAGING = YES
LIBOLED_INSTALL_TARGET = YES

$(eval $(autotools-package))
