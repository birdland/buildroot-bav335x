#!/bin/bash
if [ -z ${2} ]; then
  echo -e "\033[101m\033[97m ### ERROR: SOURCE TREE DIRECTORY NOT PASSED, PLEASE FIX MENUCONFIG TO CONTINUE ### \033[49m\033[39m"
  exit 1
else
  echo "###  Source directory specified as: '${2}'"
fi


TARGET=${1}
shift
SRCDIR=${1}
shift
MAKEDIR=${TARGET}/..
BOARDDIR=${SRCDIR}/board/birdland/bav335x
#SRCDIR=${TARGET}/../../buildroot-dev


# 201605 Gilles: Add breadcrumbs info on how this was built
# First get the git commit ID and branches
#GITLOG=`(cd ${SRCDIR} && git log -1)`
#GITBID=`(cd ${SRCDIR} && git log -1 | grep commit | awk '{ print $2 }')`
echo
GITBRANCH=`(cd "${SRCDIR}" && git branch | grep ^\* | awk '{ print $2 }')`
GITHASH=`(cd "${SRCDIR}" && git rev-parse ${GITBRANCH})`
GITMASTER=`(cd "${SRCDIR}" && git show-branch -a | ack '\*' | ack -v ${GITBRANCH} | head -n1 | sed 's/.*\[\(.*\)\].*/\1/' | sed 's/[\^~].*//')`
GITBASE=${GITMASTER%%/*}
echo "Found git repo information: Master:${GITMASTER} - Base:${GITBASE} - Branch:${GITBRANCH} - Hash:${GITHASH}"
if [ "${GITHASH}z" == "z" ]; then
  echo -e "\033[91m! Error: Could not retreive git commit ID (Verify that $(TOPDIR) Points to a git repo) !\033[39m"
  GIT_CONFIG_FILENAME="buildroot-unknown.conf"
else
  GIT_CONFIG_FILENAME="buildroot-${GITBRANCH}-${GITHASH}.conf"
fi

# Leave git breadcumbs and .config info
echo -e "\033[1mSaving buildroot .config file as '${GIT_CONFIG_FILENAME}'\033[0m"
cp "${MAKEDIR}/.config" "${TARGET}/etc/${GIT_CONFIG_FILENAME}"
rm -f "${TARGET}/etc/buildroot.conf.gz" || true
(cd "${TARGET}/etc" && zip -9v "buildroot.conf.gz" "${GIT_CONFIG_FILENAME}")
rm -f "${TARGET}/etc/${GIT_CONFIG_FILENAME}" || true
chmod 600 "${TARGET}/etc/buildroot.conf.gz" || true


# Patch rootfs with our overlay files
echo
echo ">>> Applying fixes specific to systemd init"
rsync -aqx "${BOARDDIR}/rootfs-sysd-fixes/" "${TARGET}/"

echo ">>> Create systemd mount point if missing..."
if [ ! -d "${TARGET}/run" ]; then
  mkdir ${TARGET}/run
fi
echo


# Install additional custom-built packages (passed as extra args after $2)
# Package consist of all the files under ${BOARDDOR}/packages/<package_name> for each arg after $2
for PKG_NAME in $@ ; do
  if [ ! -d "${BOARDDIR}/packages/${PKG_NAME}" ]; then
    echo -e "\033[1m\033[91mERROR: Could not find/install additional Package '$PKG_NAME'\033[39m\033[0m" && false
    exit 1
  else
    echo -e "\033[1m\033[92mInstalling Additional Package: '${PKG_NAME}'\033[0m\033[39m"
    rsync -aqx --exclude "post-install.sh" --exclude README "${BOARDDIR}/packages/${PKG_NAME}/" "${TARGET}/" || true
    if [ -f "${BOARDDIR}/packages/${PKG_NAME}/post-install.sh" ]; then
      echo "* running package provided post-install script"
      "${BOARDDIR}/packages/${PKG_NAME}/post-install.sh" "${TARGET}" "${SRCDIR}"
    else
      echo "- package has no script, all done installing '${PKG_NAME}'"
    fi
  fi
done


echo
echo ">>> Set /bin/bash as Shell (instead of sh)..."
if [ -f ${TARGET}/etc/passwd ]; then
  sed -e '/^root/s/\/bin\/sh/\/bin\/bash/g' ${TARGET}/etc/passwd > ${TARGET}/etc/passwd.tmp && mv -f ${TARGET}/etc/passwd.tmp ${TARGET}/etc/passwd
fi

if [ -d ${TARGET}/etc/init.d/ ]; then
  echo "- removing SystemV related files (/etc/init.d/)"
  rm -f "${TARGET}/etc/init.d/*" || true
fi
if [ -f ${TARGET}/etc/network/interfaces ]; then
  echo "- removing unused /etc/network/interfaces"
  rm -f "${TARGET}/etc/network/interfaces" || true
fi

# Add tty1 for LCD display
#if [ -d ${TARGET}/etc/systemd/system/getty.target.wants ]; then
#  echo "- Enabling tty1 on LCD"
#  ln -s ../../../../lib/systemd/system/getty\@.service ${TARGET}/etc/systemd/system/getty.target.wants/getty\@tty1.service
#fi

echo "- Disabling unwanted services"
rm -f ${TARGET}/etc/systemd/system/sysinit.target.wants/psplash-stop.service || true
rm -f ${TARGET}/etc/systemd/system/sysinit.target.wants/psplash-start.service || true

echo

