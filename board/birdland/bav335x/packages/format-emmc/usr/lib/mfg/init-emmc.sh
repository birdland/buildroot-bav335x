#!/bin/bash -e
# S60initemmc  Manufacturing script to program eMMC
# Detects built-in eMMC, formats and writes boot image to it.
# 
# Copyright Gilles Gameiro 2013-2014 <gilles@gigadevices.com>
# Copyright Birdland Audio 2014 <gilles.gameiro@birdland.com>

##################################################################
#
# SETUP PROGRAMMING FILES
#
# UBOOT_IMG is the tarball of the production uboot partition
# ROOTFS_IMG is a tarball of the production linux rootfs system
#
##################################################################
# WE RECOMMEND USING SYMLINKS RATHER THAN CHANGING THE LINES BELOW
# If the device tree is a symlink, this script will use the actual
# name of the file when copying to boot and configuring uEnv.txt
PREFIX=/usr/lib/mfg
DTB_FILE=${PREFIX}/bav335x-custom.dtb
UBOOT_IMG=${PREFIX}/uboot.tgz
KERNEL_IMG=${PREFIX}/zImage
ROOTFS_IMG=${PREFIX}/rootfs.tgz


# Board Model Name and Version
SERIALEE_BUS=0
SERIALEE_ADDR=0x50
SERIAL_MAGIC="0xaa 0x55 0xba 0xbe"
BOARD_NAME="0x42 0x41 0x56 0x33 0x33 0x35 0x34 0"
BOARD_VERSION="0x30 0x42 0x32 0x30"



###################################################################
#
# VARIABLES BELOW SHOULD NOT BE CHANGED IF USING THE CORRECT IMAGE
#
###################################################################
MOUNT=/mnt
EMMC=/dev/mmcblk1
OLED=/tmp/oled1305
SYSLEDS=/sys/class/leds
LED0=${SYSLEDS}/bav335x:led:usr0
LED1=${SYSLEDS}/bav335x:led:usr1
LED2=${SYSLEDS}/bav335x:led:usr2
LED3=${SYSLEDS}/bav335x:led:usr3
LEDS_PID=

# Serial EEProm Definitions
SERIAL_BUS=0
SERIAL_ADDR=0x50
SERIAL_MAGIC="0xaa 0x55 0xba 0xbe"

#if [ -L ${DTB_FILE} ]; then
#  DTB_FILE=`ls -l ${DTB_FILE} | awk '{ print $11}'`
#fi


print() {
  if [ -p ${OLED} ]; then
    echo "${1}" > ${OLED} || :
  fi
}

check_files() {
  for FILE in "$@"
  do
    if [! -e ${FILE}]; then
      echo "!!! REQUIRED FILE ${FILE} IS MISSING: VERIFY /lib/mfg contents"
      print "\5!!! MISSING FILE !!!"
      print "\4VERIFY /LIB/MFG CONTENTS"
      exit 1
    fi
  done
}


# Prevent never ending tar warnings 'date is in the future'
set_date() {
  # We should check if ntpd worked and only do this if it failed...
  # Although we'd have to block on ntp for this to be correct so...
  NEW_DATE=`ls -l ${ROOTFS_IMG} | awk '{ print $6 " " $7 " " $8+1}'`
  echo
  echo "###  Setting Date to that of archive +1 year: ${NEW_DATE} to prevent future messages"
  date -s "${NEW_DATE}"
}


init_serialee() {
  echo "###  Programming Serial EEProm with Model#"
  print "\4Programming SerialEE"
  i2cset -y ${SERIALEE_BUS} ${SERIALEE_ADDR} 0 0 ${SERIAL_MAGIC} ${BOARD_NAME} ${BOARD_VERSION} i
}


# Evaluate size of the eMMC. As of this writing only 2G and 4G has been tested but 8G is suppored
detect_size() {
  MMCSZ=`lsblk -l | awk '/^mmcblk1 / {if ($4 > 7) print "8GB"; else if ($4 > 3) print "4GB"; else if ($4 > 1) print "2GB" }' -`
  HALFSIZE=`lsblk -l | awk '/^mmcblk1 / {if ($4 > 7) print "3662M"; else if ($4 > 3) print "1795M"; else if ($4 > 1) print "902M" }' -`
  echo
  echo "###  eMMC card size detected as [${MMCSZ}]"
  print "\4 eMMC size is ${MMCSZ}"
  # List all the partitions to create, an additional partition will fill up the rest of the space available
  PARTITIONS=("16M" ${HALFSIZE})
  export MMCSZ
  export PARTITIONS
}


# Create the appropriate eMMC partitions for the detected size
partition_emmc() {
  echo 1 > ${LED0}/brightness
  print "\5- CREATING PARTITIONS"
  echo
  echo "###  Erasing eMMC bootsector"
  dd if=/dev/zero of=${EMMC} bs=512 count=8
  sync || :
  echo

  PARTS=${#PARTITIONS[@]}
  echo "###  Partitionning eMMC with ${PARTS}+1 Partitions"
  CMD="n\np\n1\n\n+${PARTITIONS[0]}\n"
  if [ $PARTS -gt "1" ]; then
    CMD="${CMD}n\np\n2\n\n+${PARTITIONS[1]}\n"
  fi
  if [ $PARTS -gt "2" ]; then
    CMD="${CMD}n\np\n3\n\n+${PARTITIONS[2]}\n"
  fi
  CMD="${CMD}t\n1\ne\na\n1\nn\np\n\n\n\nw\n"
  echo ">>> Partitioning with: '${CMD}'"
  echo -e "${CMD}" | fdisk ${EMMC} || true
  echo -e "p\nw\n" | fdisk ${EMMC}
}


# Format the eMMC paritions
format_emmc() {
  echo
  echo "###  Formating eMMC: DOS FAT Partition"
  mkfs.fat -F 16 ${EMMC}p1 -n BOOT
  # When debugging the script we can get here after the partition has been mounted once causing this to fail
  # it's probably an overkill but it would be nice to unmount any $EMMC partitions
 
  PARTS=${#PARTITIONS[@]}
  if [ "$PARTS" -gt "0"]; then 
    echo 0 > ${LED0}/brightness
    echo 1 > ${LED1}/brightness
    echo
    echo "###  Formating eMMC: 2nd partition (1st ext4)"
    mkfs.ext4 -q -F -F ${EMMC}p2 -L rootfs 
  fi
            
  if [ "$PARTS" -gt "1"]; then
    echo 0 > ${LED1}/brightness
    echo 1 > ${LED2}/brightness
    echo
    echo "###  Formating eMMC: 2nd partition (2nd ext4)"
    mkfs.ext4 -q -F -F ${EMMC}p3
  fi

  if [ "$PARTS" -gt "2"]; then
    echo 0 > ${LED2}/brightness
    echo 1 > ${LED3}/brightness
    echo
    echo "###  Formating eMMC: 3rd partition (3rd ext4)"
    mkfs.ext4 -q -F -F ${EMMC}p4
  fi
}

 
# Copy u-boot to the first bootable partition  
copy_uboot() {
  echo
  echo "###  Copying uboot files to ${EMMC}p1"
  print "\5- COPYING UBOOT TO PARTITION 1"
  mount ${EMMC}p1 ${MOUNT}

  # Copy uboot loaders
  tar zxf ${UBOOT_IMG} -C ${MOUNT}/ | true

  # Copy the Device Tree file
  echo
  echo ": Checking if '${DTB_FILE}' is a link"
  ISLNK=`ls -l ${DTB_FILE} | grep ^l`
  if [ ! -z "${ISLNK}" ]; then
    # If the device tree is a symlink, get the actual name of the file
    #FILE=`ls -l ${DTB_FILE##*/} | sed 's/.*[[:blank:]]\+\([^[:blank:]]\+\)[[:blank:]]*$/\1/'`
    FILE=`ls -l ${DTB_FILE} | sed 's/.*[[:blank:]]\+\([^[:blank:]]\+\)[[:blank:]]*$/\1/'`
    DTBNAME=${FILE##*/}
    echo ": DTB File '${FILE}' actual name is '${DTBNAME}'"
  else
    DTBNAME=${DTB_FILE##*/}
  fi
  mkdir ${MOUNT}/dtbs || true
  cp -v ${DTB_FILE} ${MOUNT}/dtbs/${DTBNAME}

  # Patch uEnv and copy it
  print "\4- Patch uEnv for dtb.."
  sed -i "s/^fdtfile.*/fdtfile=dtbs\/${DTBNAME}/" ${MOUNT}/uEnv.txt

  # Copy zImage
  print "\4- Copying Kernel.."
  cp -v ${KERNEL_IMG} ${MOUNT}
  umount ${EMMC}p1
}


# Copy the rootfs system to the default system partition
copy_rootfs() {
  echo
  echo "###  Copying rootfs files to ${EMMC}p2"
  print "\5 COPYING ROOT FILE SYSTEM"
  mount ${EMMC}p2 ${MOUNT}
  tar zxf ${ROOTFS_IMG} -C ${MOUNT}/ | true
  echo
  echo "###  Flushing writes to ${EMMC}p2"
  umount ${EMMC}p2 
}


# Remove trigeers from all LED
leds_init() {
  # rc-feedback nand-disk mmc0 mmc1 timer oneshot heartbeat backlight gpio cpu0 default-on
  leds_stop_blink
  echo none > ${LED0}/trigger
  echo none > ${LED1}/trigger
  echo none > ${LED2}/trigger
  echo none > ${LED3}/trigger
  echo 0 > ${LED0}/brightness
  echo 0 > ${LED1}/brightness
  echo 0 > ${LED2}/brightness
  echo 0 > ${LED3}/brightness
}


# blink_leds restores trigger access to uSD/eMMC LEDS and altenatively binks 0 and 1 until killed
_leds_blink_thread() {
  # indicate uSD and eMMC access activity on LED2 and LED3
  echo mmc0 > ${LED2}/trigger
  echo mmc1 > ${LED3}/trigger
  
  while true; do
    echo 1 > ${LED1}/brightness
    echo 0 > ${LED0}/brightness
    sleep .5
    echo 1 > ${LED0}/brightness
    echo 0 > ${LED1}/brightness
    sleep .5
  done
}

leds_start_blink() {
  _leds_blink_thread & LEDS_PID=$!
  export LEDS_PID;
}

leds_stop_blink() {
  [ -e /proc/$LEDS_PID ] || kill $LEDS_PID
  LEDS_PID=
  export LEDS_PID
}


# Blink all LEDs together (inidicates an error in programming)
leds_error() {
  leds_init
  echo "###########################################################"
  echo "###  FAILED TO DETECT eMMC. BOARD CANNOT BE PROGRAMMED  ###"
  echo "###########################################################"
  while true; do
    echo 0 > ${LED0}/brightness
    echo 0 > ${LED1}/brightness
    echo 0 > ${LED2}/brightness
    echo 0 > ${LED3}/brightness
    sleep .5
    echo 1 > ${LED0}/brightness
    echo 1 > ${LED1}/brightness
    echo 1 > ${LED2}/brightness
    echo 1 > ${LED3}/brightness
    sleep .5
  done
}


# Turn all LEDs on (indicates successful completion)
leds_allon() {
  leds_init
  echo 1 > ${LED0}/brightness
  echo 1 > ${LED1}/brightness
  echo 1 > ${LED2}/brightness
  echo 1 > ${LED3}/brightness
}
  
    



init_emmc() {
  # Program the Serial EEProm Model# (Serial and Mac are done elsewhere)
  init_serialee

  # remove all triggers from LEDs
  leds_init
  
  # detect the size of the eMCC on the board
  detect_size
  if [ -z $MMCSZ ]; then
    print "\5 Could not Detect EMMC Size"
    leds_error
  fi
  # paranoid
  [ -z $MMCSZ ] && exit -1
  
  # Change date to prevent tar warnings
  set_date
  
  # Create the appropriate partitions for detected size
  partition_emmc
  
  # and format the eMMC
  format_emmc
  
  # alternate LED0 and 1 during copying of the files
  leds_start_blink
  
  # copy the u-boot boot files to the first bootable partition
  copy_uboot
  
  # and copy the system rootfs to the default system partition
  copy_rootfs
  
  # and turn all LEDs on to indicate the programing is complete 
  leds_allon
  sleep 1
  print "\5 EMMC IS PROGRAMMED"
}


#case "$1" in
#	start)
		init_emmc
#		;;
#	stop)
#		;;
#	*)
#		echo "usage: $0 {start|stop|restart}"
#		;;
#esac

