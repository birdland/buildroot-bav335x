#!/bin/bash
TARGET=$1
BOARDDIR=${TARGET}/../../buildroot-dev/board/birdland/bav335x

echo ">>> Applying fixes specific to System-V init"
rsync -avx ${BOARDDIR}/rootfs-sysv-fixes/ ${TARGET}/

# This needs to be added manually because it's built but not installed (too big)
echo ">>> Adding SystemV daemon utilities..."
cp ${TARGET}/../staging/usr/sbin/start-stop-daemon ${TARGET}/usr/sbin
#cp -r ${TARGET}/../staging/usr/share/man ${TARGET}/usr/share
