#!/bin/bash
TARGET=$1
BOARDDIR=${TARGET}/../../buildroot-dev/board/birdland/bav335x

source ${BOARDDIR}/postbuild-sysd.sh

echo ">>> Adding Birdland Manufacturing files"
rsync -avx ${BOARDDIR}/rootfs-manuf/ ${TARGET}/


# Enable root auto login on console on Sys-V
if [ -f ${TARGET}/etc/inittab ]; then
  echo ">>> Enable auto-login for the manufacturing console (SysV)"
  sed -i "s/^O0\:\:respawn\:\/sbin\/getty.*/O0\:\:respawn\:\/sbin\/getty -a root -L ttyO0 115200 vt100" ${TARGET}/etc/inittab
fi

# Enable root auto login on console on systemd
if [ -f "${TARGET}/lib/systemd/system/serial-getty@.service" ]; then
  echo ">>> Enable root auto-login on serial console (systemd)"
  sed  -i "s/^\(Exec.*getty\)\(.*\)\(-L.*\)/\1 -a root \3/" "${TARGET}/lib/systemd/system/serial-getty@.service"
fi


# Delete invocation to manufacturing script if it's already on target and add it again
echo ">>> Appending eMMC Programming script to root profile"
sed -i "/program-emmc/d" ${TARGET}/etc/profile
sed -i "\$a\source \/lib\/mfg\/program-emmc\.sh" ${TARGET}/etc/profile

